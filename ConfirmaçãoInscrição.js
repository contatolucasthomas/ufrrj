function matricula() {

  const planilha = SpreadsheetApp.getActiveSpreadsheet()
  const pagina = planilha.getSheetByName('Respostas ao formulário 1')
  var dados = pagina.getDataRange().getValues()
  var mensagem = ""
  var assunto = "Boas vindas 2021!!! "

  const turma_id = "270717944889"

  for(var x=0 ; x < dados.length ; x++)
  {
    var linha = dados[x]

    var nome = linha[1]
    var email = linha[3]
    var status = linha[4]

    if (status == '' || status == undefined)
    {
          
      try{
        var resource = {
            "courseId": turma_id,
            "userId": email
        }
        var r = Classroom.Courses.Students.create(resource,turma_id)
        // Enviando mensagem de boas vindas
        mensagem += "Olá <b>"+nome+"<b> <br><br>"
        mensagem += "<h5>Seja bem vindo ao nosso curso!<h5> <br><br>"
        mensagem += "Cordialmente, <br><br>"
        mensagem += "UFRRJ"
        GmailApp.sendEmail(email,assunto,"",{htmlBody:mensagem})
        pagina.getRange(x+1,5).setValue("Processado!").setFontColor('#008000')
      }catch(er){
        Logger.log(er)
        pagina.getRange(x+1,5).setValue("Erro"+e).setFontColor('#FF0000')
      }

    }
  } // for dados
}
